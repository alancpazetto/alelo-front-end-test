import { Component, OnInit } from '@angular/core';
import { ListService } from '../services/list/list.service';
import { ListType } from '../interfaces/list-type';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [ListService]
})
export class HomeComponent implements OnInit {

  arrListTypes: Array<ListType>;

  showLoading: Boolean = false;

  constructor(
    private listService: ListService
  ) {

    this.showLoading = true;
    listService.getListTypes()
      .subscribe( res => {
        this.showLoading = false;
        this.arrListTypes = res;
      });

  }

  ngOnInit() {
  }

}
