import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-new-item',
  templateUrl: './add-new-item.component.html',
  styleUrls: ['./add-new-item.component.scss']
})
export class AddNewItemComponent implements OnInit {

  @Input() title: String;
  @Input() alertTitle: String = 'Preencha o campo corretamente';
  @Input() buttonTitle: String;
  @Input() placeholderText: String;

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClickAddItem: EventEmitter<String> = new EventEmitter();

  inputItem: String = '';

  constructor() { }

  ngOnInit() {
  }

  onClickAddItemInView(): void {

    if ( this.inputItem.trim().length > 0 ) {
      this.onClickAddItem.emit( this.inputItem );
      this.inputItem = '';
    } else {
      alert( this.alertTitle );
    }

  }

}
