import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ListType } from '../../interfaces/list-type';
import { environment } from '../../../environments/environment';
import { List } from '../../interfaces/list';
import { RequestOptions, Headers } from '@angular/http';
import { ListItem } from '../../interfaces/list-item';

@Injectable()
export class ListService {

  constructor( private http: HttpClient ) { }

  getListTypes (): Observable< Array<ListType> > {

    const url = environment.WsUrl + 'Lists/listTypes';
    return this.http.get< Array<ListType> >(url);

  }

  getListsByType (typeId: Number): Observable< Array<List> > {

    const url = environment.WsUrl + 'Lists/byTypeId/' + typeId;
    return this.http.get< Array<List> >(url);

  }

  getListById (listId: Number): Observable< List > {

    const url = environment.WsUrl + 'Lists/getListById/' + listId;
    return this.http.get< List >(url);

  }

  postList (list: List): Observable<void> {

    const url = environment.WsUrl + 'Lists/saveList';

    const headers = new HttpHeaders({ 'Content-Type': 'text/plain' });

    return this.http.post<void> (url, {
      typeId: list.typeId,
      name: list.name
    }, { headers: headers });

  }

  postListItem( listItem: ListItem ): Observable<void> {

    const url = environment.WsUrl + 'Lists/saveListItem';
    const headers = new HttpHeaders({ 'Content-Type': 'text/plain' });

    return this.http.post<void> (url, {
      listId: listItem.listId,
      name: listItem.name
    }, { headers: headers });

  }

  removeItem (itemId: Number): Observable<void> {

    const url = environment.WsUrl + 'Lists/removeItem/' + itemId;
    return this.http.get<void>(url);

  }

  removeList (listId: Number): Observable<void> {

    const url = environment.WsUrl + 'Lists/removeListAndItems/' + listId;
    return this.http.get<void>(url);

  }

}
