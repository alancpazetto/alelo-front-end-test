import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListItem } from '../interfaces/list-item';
import { ListService } from '../services/list/list.service';
import { List } from '../interfaces/list';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss'],
  providers: [ ListService ]
})
export class ListItemsComponent implements OnInit {

  showLoading: Boolean = false;

  list: List = {
    items: []
  };

  listTypeId: Number;
  listId: Number;

  constructor(
    private route: ActivatedRoute,
    private listService: ListService
  ) {

    route.params.subscribe( params => {
      this.listId = params.listId;
      this.listTypeId = params.listTypeId;

      this.loadItems();

    });

  }

  ngOnInit() {
  }

  loadItems(): void {

    this.showLoading = true;
    this.listService.getListById( this.listId )
      .subscribe( res => {
        this.showLoading = false;
        this.list = res;
      });

  }

  onAddNewItem( newItem: String ): void {

    const newListItem: ListItem = {
      name: newItem,
      listId: this.listId
    };

    this.listService.postListItem( newListItem )
      .subscribe( res => {
        alert('Item adicionado com sucesso');
        this.loadItems();
      });

  }

  onClickRemoveItem( item ): void {

    if ( confirm('Remover o item "' + item.name + '"?') ) {
      this.listService.removeItem( item.id )
        .subscribe( res => {
          alert('Item removido com sucesso');
          this.loadItems();
        });
    }

  }

}
