import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { environment } from '../environments/environment';
import { HomeComponent } from './home/home.component';
import { ListsComponent } from './lists/lists.component';
import { ListItemsComponent } from './list-items/list-items.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { AddNewItemComponent } from './add-new-item/add-new-item.component';
import { LoadingComponent } from './loading/loading.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'lists/:listTypeId', component: ListsComponent },
  { path: 'lists/:listTypeId/items/:listId', component: ListItemsComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    HomeComponent,
    ListsComponent,
    ListItemsComponent,
    PageHeaderComponent,
    AddNewItemComponent,
    LoadingComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: !environment.production }
      { enableTracing: false }
    ),
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AngularFontAwesomeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
