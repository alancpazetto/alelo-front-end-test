import { Component, OnInit } from '@angular/core';
import { ListService } from '../services/list/list.service';
import { List } from '../interfaces/list';
import { ActivatedRoute } from '@angular/router';
import { ListType } from '../interfaces/list-type';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss'],
  providers: [ ListService ]
})
export class ListsComponent implements OnInit {

  showLoading: Boolean = false;

  listTypeId: Number;
  arrLists: Array< List >;

  constructor(
    private listService: ListService,
    private route: ActivatedRoute
  ) {

    this.listTypeId = Number(route.snapshot.paramMap.get('listTypeId'));
    this.loadLists();

  }

  ngOnInit() {
  }

  loadLists(): void {
    this.showLoading = true;
    this.arrLists = [];
    this.listService.getListsByType( this.listTypeId )
      .subscribe( (res) => {
        this.showLoading = false;
        this.arrLists = res;
      });
  }

  onAddNewList(newListName: String): void {

    const newList: List = {
      id: 0,
      name: newListName,
      status: 1,
      typeId: this.listTypeId
    };

    this.listService.postList( newList )
      .subscribe( res => {
        alert('Lista adicionada com sucesso');
        this.loadLists();
      });

  }

  onClickRemoveList( list ): void {

    if ( confirm('Remover a lista "' + list.name + '"?') ) {
      this.listService.removeList( list.id )
        .subscribe( res => {
          alert('Lista removida com sucesso');
          this.loadLists();
        });
    }

  }

}
