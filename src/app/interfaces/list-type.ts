export interface ListType {
  id: Number;
  name: String;
  status: Number;
}
