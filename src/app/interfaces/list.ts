import { ListItem } from './list-item';

export interface List {
    id?: Number;
    typeId?: Number;
    type?: String;
    name?: String;
    status?: Number;
    items?: Array<ListItem>;
}
