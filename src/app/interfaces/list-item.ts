export interface ListItem {
    id?: Number;
    listId: Number;
    name: String;
}
